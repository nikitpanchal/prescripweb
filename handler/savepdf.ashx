﻿<%@ WebHandler Language="C#" Class="fileUploader" %>

using System;
using System.Web;
using System.Configuration;
public class fileUploader : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        try
        {
            HttpPostedFile postedFile = context.Request.Files[0];
            string doctorName = context.Request.Params["doctorname"].ToString();
            postedFile.SaveAs(@"D:\Prescrip\prescrip-web\pdffiles\" + doctorName + ".pdf");


        }
        catch (Exception ac)
        {

        }


    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}