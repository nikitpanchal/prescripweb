﻿$(document).ready(function () {
    otp();
    getOtp();
    changeNum();
});

function otp() {
    $('#wrapper-otp').find('input').each(function () {
        $(this).attr('maxlength', 1);
        $(this).on('keyup', function (e) {
            var parent = $($(this).parent());

            if (e.keyCode === 8 || e.keyCode === 37) {
                var prev = parent.find('input#' + $(this).data('previous'));

                if (prev.length) {
                    $(this).attr('disabled', 'disabled');
                    $(prev).select();
                }
            } else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                var next = parent.find('input#' + $(this).data('next'));

                if (next.length) {
                    $(next).removeAttr('disabled');
                    $(next).select();

                } else {
                    checkOtp();
                }
            }
        });
    });
}
var optResonse = '';
function checkOtp() {
    var inputOtp = '';
    $.each($('#wrapper-otp input'), function (key, value) {
        inputOtp += $(value).val();
    });
    if (inputOtp == optResonse) {
        window.location.href = "/views/home.aspx";
    }
}
function isNumberKey(evt) {
    $('.required-mob').hide();
    var charCode = (evt.which) ? evt.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
function changeNum() {
    $('#changenum').unbind('click').click(function () {
        optResonse = '';
        $('#wrapper-otp input[type=text]').val('');
        $('#wrapper-otp input[type=text]').not($('#wrapper-otp input[type=text]').val('').eq(0)).attr('disabled', 'disabled');


        var otpText = 'Please enter 4 - Digit code sent to you at +91' + $('#mobile').val() + "<label id='changenum' class='change-num'>Change number ?</label>";
        $('.prompt').html('');
        $('#wrapper-otp').hide();
        $('#textform').fadeIn();

    })
}

function getOtp() {
    $('#getotp').unbind('click').click(function () {
        if ($('#mobile').val().length > 0) {
            $('.required-mob').hide();
            $('#textform').hide();
            var otpText = 'Please enter 4 - Digit code sent to you at +91' + $('#mobile').val() + " <label id='changenum' class='change-num'>Change number ?</label>";
            $('.prompt').html(otpText);
            $('#wrapper-otp').fadeIn();
            $('#digit-1').focus();
            changeNum();
            $.ajax({
                url: 'http://18.140.74.211:8004/sendotp',
                type: 'POST',
                data: JSON.stringify({ "mobile": $('#mobile').val(), "pkey": "i@ILKM1!" }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',

                success: function (data, textStatus, xhr) {
                    optResonse = data.otp;

                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('Error in Operation');
                }

            });


        }
        else {
            $('.required-mob').show();
        }

    })
}
