﻿$(document).ready(function () {

    backform();
});

function formsubmitstepone() {
    $('#form1').addClass('hidden');
    $('#form3').removeClass('hidden');
    $('#backform').removeClass('hidden');
    $('#bdheader').removeClass('active-nav');
    $('#numbd').removeClass('active');
    $('.addetls').addClass('active-nav');
    $('.addetlnum').addClass('active');

}
function backform() {

    $('#backform').unbind('onclick').click(function () {
        $('#backform').addClass('hidden');
        $('#form3').addClass('hidden');
        $('#form1').removeClass('hidden');
        $('#bdheader').addClass('active-nav');
        $('#numbd').addClass('active');
        $('.addetls').removeClass('active-nav');
        $('.addetlnum').removeClass('active');
    });

}
function finishbtn() {
    window.location.href = "home.aspx";
}