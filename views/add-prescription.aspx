﻿<%@ Page Title="" Language="C#" MasterPageFile="~/views/patient.master" AutoEventWireup="true" CodeFile="add-prescription.aspx.cs" Inherits="views_add_prescription" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../js/prescription.js"></script>
    <script src="../js/pdfobject.min.js"></script>
    <script src="../js/jspdf.min.js"></script>
    <style>
        .active {
            color: #f15f0f;
            font-weight: bold;
        }

        .section-pres {
            display: none;
        }

        .prescriptionform label {
            font-size: 13px;
            color: #424242;
            margin: 0px;
            margin-bottom: 05px;
            font-family: RobotoRegular;
        }

        .navheaderlbl {
            margin-top: 0px;
            margin-bottom: 10px;
        }

        .deactive-nav {
            color: #424242;
            font-size: 15px;
            font-family: 'RobotoLight';
        }

        .active-nav {
            border: 0px !important;
            font-size: 14px;
            font-family: 'RobotoRegular' !important;
            color: #f15f0f;
            font-weight: bold;
        }

        .navsection {
            width: 130px;
        }

        .quicknavbtn {
            width: 100%;
            height: 35px;
            font-size: 14px;
            font-family: RobotoRegular;
            padding: 07px;
        }

        .form-group {
            height: 63px;
            margin-bottom: 08px;
        }

        button.btn {
            height: 45px;
            font-size: 16px;
        }

        .searchicontextbox {
            background-image: url('content/images/icon-serch-green.png');
        }

        .form {
            min-height: 350px;
        }

        .optionallighttxt {
            font-size: 12px !important;
            font-family: RobotoRegular !important;
            color: #aeaeae !important;
            font-weight: normal !important;
        }

        .dosagetime {
            color: #484848 !important;
        }

        .txttimetakenperiod {
            text-align: center;
            color: #282828;
            font-size: 14px;
            font-family: RobotoLight;
        }

        .savesearchdata {
            border: 1px solid #dfdfdf;
            min-width: 100px;
            padding: 03px 10px;
            color: #424242;
            font-family: RobotoLight;
            text-align: center;
            height: 28px;
            margin: 5px 04px;
        }

        .popover {
            max-width: 960px;
        }

            .popover .arrow {
                left: 32px !important;
            }

        .uploadednotesimagesshow {
            border: 1px solid #dfdfdf;
            width: 70px;
            height: 70px;
            margin: 0px 10px;
        }


            .uploadednotesimagesshow .imagesection {
                position: relative;
                height: 68px;
                width: 68px;
                background-size: 90%;
                background-size: cover;
                background-repeat: no-repeat;
            }


            .uploadednotesimagesshow .deleteimages {
                background-color: #fff;
                position: absolute;
                top: -12px;
                right: -12px;
                cursor: pointer;
            }

        .hidden {
            display: none;
        }

        .show {
            display: block;
        }


        .popover-area {
            position: absolute;
            z-index: 5555555;
            display: none;
        }

        .popover-body {
            background: #FBFBFB;
            padding: 0px;
        }

        .popover-body-heading {
            font-size: 13px;
            margin: 2px 10px;
            font-weight: 600;
        }

        .popover-body-data {
            height: 29px;
            background: #FBFBFB;
            padding: 4px 20px;
            border-top: 1px solid #EADCDC;
        }

            .popover-body-data:focus, .popover-body-data:active, .popover-body-data:hover {
                background: rgb(240, 240, 240) !important;
            }

        .popover-body-data-remove {
            text-align: center;
            color: red;
        }


        .leftdata {
            width: 95%;
            min-height: 20px;
        }

        .rightclose {
            height: 20px;
            width: 20px;
        }

        .popover-body-data-remove:hover {
            color: #f15f0f;
        }

        .relative {
            position: relative;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">





    <div class="clearfix"></div>


    <div class="">


        <div class="containerx" style="position: relative">



            <div style="position: absolute; width: 100%; top: 73%; height: 5px; border-top: 1px solid #cacaca;">
            </div>

            <div class="" style="width: 920px; margin: 10px auto; position: relative">

                <div class="">

                    <div class=" topquicknav">

                        <div class="pull-left" style="text-align: left; text-align: center">

                            <div class="" style="margin-top: 24px; width: 85px">
                                <input id="btnback" type="button" class="btn exclude btn-default quicknavbtn hidden" tabindex="15" onclick="backbtnclick()" value="Back" style="" />
                            </div>



                        </div>


                        <div class="pull-left " style="text-align: left; text-align: center; width: 720px">

                            <div style="width: 700px; margin: 0px auto">

                                <!-- ngRepeat: frm in formnavigations -->
                                <div id="navi1" class="pull-left navsection ng-scope" onclick="jumptoindex('#form1','#navi1',1)">
                                    <div class="no-border navheaderlbl exclude ng-binding active-nav" role="button" tabindex="0" style="">Assessment </div>
                                    <div class="top-nav-show exclude ng-binding active" role="button" tabindex="0" style="">1 </div>
                                </div>
                                <!-- end ngRepeat: frm in formnavigations -->
                                <div id="navi2" class="pull-left navsection ng-scope" onclick="jumptoindex('#form2','#navi2',2)">
                                    <div class="no-border navheaderlbl exclude ng-binding deactive-nav" role="button" tabindex="0" style="">Prescription </div>
                                    <div class="top-nav-show exclude ng-binding deactive" role="button" tabindex="0" style="">2 </div>
                                </div>
                                <!-- end ngRepeat: frm in formnavigations -->
                                <div id="navi3" class="pull-left navsection ng-scope" onclick="jumptoindex('#form3','#navi3',3)">
                                    <div class="no-border navheaderlbl exclude ng-binding deactive-nav" role="button" tabindex="0">Follow up </div>
                                    <div class="top-nav-show exclude ng-binding deactive" role="button" tabindex="0">3 </div>
                                </div>
                                <!-- end ngRepeat: frm in formnavigations -->
                                <div id="navi4" class="pull-left navsection ng-scope" onclick="jumptoindex('#form4','#navi4',4)">
                                    <div class="no-border navheaderlbl exclude ng-binding deactive-nav" role="button" tabindex="0">Display preferences </div>
                                    <div class="top-nav-show exclude ng-binding deactive" role="button" tabindex="0">4 </div>
                                </div>
                                <!-- end ngRepeat: frm in formnavigations -->
                                <div id="navi5" class="pull-left navsection ng-scope" onclick="jumptoindex('#form5','#navi5',5)">
                                    <div class="no-border navheaderlbl exclude ng-binding deactive-nav" role="button" tabindex="0">Preview &amp; Finish </div>
                                    <div class="top-nav-show exclude ng-binding deactive" role="button" tabindex="0">5 </div>
                                </div>
                                <!-- end ngRepeat: frm in formnavigations -->






                                <div class="clearfix"></div>

                            </div>

                        </div>

                        <div class="pull-right" style="text-align: left; text-align: center">
                            <div class="" style="margin-top: 24px; width: 85px">
                                <input id="btnnext" type="submit" onclick="nextbtnclick()" tabindex="14" accesskey="s" class="btn btn-default quicknavbtn  exclude " value="Next" />
                            </div>

                        </div>

                        <div class="clearfix"></div>


                    </div>


                </div>
            </div>

        </div>

        <div class="row" style="width: 920px; margin: 40px auto;">
            <div class="section-pres" id="form1">
            </div>
            <div class="section-pres" id="form3">
            </div>
            <div class="section-pres" id="form4">
            </div>
            <div class="section-pres" id="form5">
                <div class="pull-left" style="width: 610px; border: 1px solid #a9a9a9; box-shadow: 2px 2px 2px rgba(169, 169, 169, 0.66); height: 690px; border-radius: 2px">

                    <iframe type="application/pdf" src="http://radiantmobile.prescrip.in/template/report.html?patientid=5edb301902b9141aa0d5d0ba" id="pdfdata" style="width: 100%; height: 100%; border: 0px"></iframe>



                </div>

                <div class="pull-right">

                    <div style="margin-top: 63px;">

                        <a href="http://radiantmobile.prescrip.in/template/report.html?TemplatebodyType=3&amp;HeaderType=1&amp;FooterType=1&amp;webprint=1&amp;patientid=5ede358302b9141aa0d5eea1" target="_blank" class="btn btn-default icon-print-icon exclude"
                            tabindex="3" id="btnprintpresc" style="width: 240px; height: 56px; font-size: 16px; line-height: 36px;"
                            onclick="printpdf()">Print Prescription</a>

                    </div>

                    <div style="margin-top: 10px;">
                        <a href="home.aspx" class="btn btn-default icon-save-icon exclude" tabindex="4" style="width: 240px; height: 56px; font-size: 16px; line-height: 36px; margin-bottom: 10px;">Save  & Finish </a>

                    </div>

                    <a style="margin-top: 10px;">
                        <button class="btn btn-default Select-pharmacy-icon exclude" tabindex="5" style="width: 240px; height: 56px;" ng-click="phramacyclick()">Template settings</button>

                    </a>



                </div>
            </div>
            <div id="form2" class="section-pres">

                <div class="" style="margin: 05px 0px">
                    <div class="pull-left" style="width: 180px; height: 67px">

                        <div style="position: relative">
                            <label>DOSAGE FORM</label>
                            <input maxlength="100" tabindex="3" id="txtdosageform" ng-model="txtdosageform" readonly="readonly" placeholder="Select" type="text"
                                onclick="opendosageform('#dosagesel')"
                                class="form-control  select exclude" style="height: 27px;" />


                            <span class="errrotext ">{{ txtdosageformerror }}

                            </span>



                            <!--- Dosage Form section start-->
                            <div id="dosagesel" class=" exclude popover-area {{pagepophover.dosageformisopen==true?pagepophover.show:pagepophover.hidden}}">
                                <div class=" exclude popover ng-isolate-scope bottom fade in bottom-left" style="display: block">
                                    <div class="arrow exclude" style="top: -11px; left: 6px;"></div>

                                    <div style='width: 920px' class="popover-content exclude">
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-title exclude">
                                            <div class="row exclude">
                                                <div class="col-xs-12 col-md-12 col-lg-12 exclude">
                                                    <input type="text" name="txtsearchdosageform" maxlength="100" id="txtsearchdosageform" placeholder="search" ng-model="txtsearchdosageform"
                                                        ng-keyup="CityStateSearch($event,'dosageform')"
                                                        ng-keypress="dosageformlimit=15;prevententerd($event,'dosageform');"
                                                        class="form-control exclude" autofocus />

                                                </div>
                                            </div>
                                        </div>
                                        <div class="exclude col-xs-12 col-md-12 col-lg-12 popover-body">
                                            <div class="exclude" style="height: 200px; overflow-y: auto" ng-init="dosageformlimit=10;" id="btndosageadd" buffered-scroll=" (dosageformlimit<lazyloadingpage.dosageform.data.length)? dosageformlimit=dosageformlimit+150:false">
                                                <!--  <!--Recent start -->
                                                <!-- <div class="exclude" ng-show="((txtsearchdosageform.length<=0 || txtsearchdosageform==undefined)  && lazyloadingpage.dosageform.recent.length>=1)">

                                                     <div class="popover-body-heading exclude">Recently selected</div>
                                                     <div class="popover-body-data exclude" ng-repeat="item in lazyloadingpage.dosageform.recent  | filter:{isrecent:1}:true | orderBy:'whenupdate':true | limitTo:10" ng-click="selecteddosagedata(item,0,$event);">
                                                         <div class="pull-left exclude"> {{item.DosageForm}} </div>
                                                         <div class="clearfix exclude"></div>
                                                     </div>

                                                 </div>-->
                                                <!--Recent start end -->
                                                <!--Normal start -->
                                                <div class="exclude">
                                                    <!--                                                    <div class="popover-body-heading exclude"> Dosage From </div>-->
                                                    <div class="popover-body-data exclude dosagformlistcount" onclick="dosageformselected('#brandsel')" id="mydosageform{{$index}}">
                                                        <div class="pull-left exclude">Tablet </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="popover-body-data exclude dosagformlistcount" onclick="dosageformselected('#brandsel')" id="mydosageform{{$index}}">
                                                        <div class="pull-left exclude">Capsule </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="popover-body-data exclude dosagformlistcount" onclick="dosageformselected('#brandsel')" id="mydosageform{{$index}}">
                                                        <div class="pull-left exclude">Syrup </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <!--Normal start -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!---Dosage Form section End-->

                        </div>



                    </div>
                    <div class="pull-left" style="width: 180px; margin-left: 05px; height: 67px">

                        <div style="position: relative">
                            <label>BRAND / GENERIC NAME</label>

                            <input maxlength="100" id="txtgenericname" name="txtgenericname"
                                readonly="readonly" tabindex="4" ng-model="txtgenericname" placeholder="Select"
                                ng-enter="pagepophover.enable($event,'dosebrandisopen')"
                                onclick="opendosageform('#brandsel')"
                                ng-keydown="pagepophover.enable($event,'dosebrandisopen')"
                                type="text" class="form-control  select exclude " style="height: 27px;" />



                            <span class="errrotext ">{{txtgenericnameerror }}

                            </span>



                            <!--- Dose Brand and Regimen search  section start-->

                            <div id="brandsel" class="popover-area exclude {{pagepophover.dosebrandisopen==true?pagepophover.show:pagepophover.hidden}}">
                                <div class="popover exclude ng-isolate-scope bottom fade in bottom-left" style="display: block">
                                    <div class="arrow exclude" style="top: -11px; left: 6px;"></div>

                                    <div style='width: 733px' class="popover-content exclude">
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-title exclude">
                                            <div class="row exclude">
                                                <div class="col-xs-8 col-md-8 col-lg-10 exclude">
                                                    <input type="text" id="txtsearchbrand" name="txtsearchbrand" maxlength="100"
                                                        ng-model="txtsearchbrand" class="form-control exclude" placeholder="Search"
                                                        ng-keyup="CityStateSearch($event,'dosebrand')"
                                                        ng-keypress="limitbrand=15;prevententerd($event,'dosebrand');"
                                                        autofocus />
                                                </div>
                                                <div class="col-xs-4 col-md-4 col-lg-2 exclude">
                                                    <input type="button" id="btnaddbrand" class=" btn btn-primary col-xs-12 col-md-12 col-lg-12 form-control exclude" value="Add" ng-click="addnewbrandgeneric(txtsearchbrand);" style='padding: 2px 10px' my-enter="addnewbrandgeneric(txtsearchbrand)" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-body exclude">
                                            <div class="exclude" style="height: 200px; overflow-y: scroll" ng-init="limitbrand=15;" id="Div3" buffered-scroll="(!lazyloadingpage.genericasyncload(this)? limitbrand=limitbrand+150 :( (limitbrand<lazyloadingpage.brandsgeneric.data.length)? limitbrand=limitbrand+150:false))">

                                                <!--Recent start end -->
                                                <!--Normal start -->
                                                <div class="exclude" ng-show="((txtsearchbrand.length>0 || txtsearchbrand==undefined) && lazyloadingpage.brandsgeneric.data.length>=1)">

                                                    <!-- <div class="popover-body-heading exclude hidden" > Brand </div>-->
                                                    <div class="popover-body-data exclude dosagebrandlistcount"
                                                        id="mydosagebrand{{$index}}">
                                                        <div class="pull-left exclude leftdata" onclick="selectedbranddata()">Crocin</div>
                                                        <div class="pull-right exclude rightclose" ng-show="item._id" ng-click="removebranddata(item)">
                                                            <span class="exclude glyphicon glyphicon-minus-sign  popover-body-data-remove "></span>
                                                        </div>

                                                        <div class="clearfix exclude"></div>
                                                    </div>
                                                    <div class="popover-body-data exclude dosagebrandlistcount"
                                                        id="mydosagebrand{{$index}}">
                                                        <div class="pull-left exclude leftdata" onclick="selectedbranddata()">Crocin</div>
                                                        <div class="pull-right exclude rightclose" ng-show="item._id" ng-click="removebranddata(item)">
                                                            <span class="exclude glyphicon glyphicon-minus-sign  popover-body-data-remove "></span>
                                                        </div>

                                                        <div class="clearfix exclude"></div>
                                                    </div>
                                                    <div class="popover-body-data exclude dosagebrandlistcount"
                                                        id="mydosagebrand{{$index}}">
                                                        <div class="pull-left exclude leftdata" onclick="selectedbranddata()">Crocin</div>
                                                        <div class="pull-right exclude rightclose" ng-show="item._id" ng-click="removebranddata(item)">
                                                            <span class="exclude glyphicon glyphicon-minus-sign  popover-body-data-remove "></span>
                                                        </div>

                                                        <div class="clearfix exclude"></div>
                                                    </div>
                                                </div>
                                                <!--Normal start -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!--- Dose Brand and Regimen  search  section End-->

                        </div>


                    </div>
                    <div class="pull-left" style="width: 180px; margin-left: 05px; height: 67px">

                        <div style="position: relative">
                            <label>DOSE</label>
                            <input maxlength="100" tabindex="5" id="txtdose" name="txtdose" readonly="readonly" ng-model="txtdose" placeholder="Select"
                                ng-enter="pagepophover.enable($event,'ddoseisopen')"
                                onclick="opendosageform('#dosesel')"
                                ng-keydown="pagepophover.enable($event,'ddoseisopen')"
                                type="text" class="form-control  select  exclude" style="height: 27px;" />
                            <span class="errrotext ">{{txtdoseerror }}
                            </span>

                            <!--- Dose  search  section start-->
                            <div id="dosesel" class="popover-area exclude {{pagepophover.ddoseisopen==true?pagepophover.show:pagepophover.hidden}}">
                                <div class="popover exclude ng-isolate-scope bottom fade in bottom-left exclude" style="display: block">
                                    <div class="arrow exclude" style="top: -11px; left: 6px;"></div>

                                    <div style='width: 548px' class="popover-content exclude">
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-title exclude">
                                            <div class="row exclude">
                                                <div class="col-xs-8 col-md-8 col-lg-10 exclude">
                                                    <input type="text" name="txtsearchddose" maxlength="100" id="txtsearchddose"
                                                        placeholder="search" ng-model="txtsearchddose"
                                                        ng-keyup="CityStateSearch($event,'dosagedose')"
                                                        ng-keypress="limitddose=15;prevententerd($event,'dosagedose');"
                                                        class="form-control exclude" autofocus />
                                                </div>
                                                <div class="col-xs-4 col-md-4 col-lg-2 exclude">
                                                    <input type="button" id="btnddoseadd" class="exclude btn btn-primary col-xs-12 col-md-12 col-lg-12 form-control" value="Add" ng-click="addnewdosedata(txtsearchddose);" style='padding: 2px 10px' my-enter="addnewdosedata(txtsearchddose)" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-body exclude">
                                            <div class="exclude" style="height: 200px; overflow-y: scroll" ng-init="limitbrand=15;" id="Div4" buffered-scroll="(!lazyloadingpage.genericasyncload(this)? limitddose=limitbrand+150 :( (limitbrand<lazyloadingpage.brandsgeneric.data.length)? limitddose=limitddose+150:false))">
                                                <!--Normal start -->
                                                <div class="exclude">
                                                    <div class="popover-body-data exclude  dosagedoselistcount" ng-repeat="item in lazyloadingpage.dosagedose.data  |  filter:txtsearchddose:false |  orderBy:'dose'  | limitTo:limitddose" ng-repeat-steps="50"
                                                        data-val="{{item.dose}}" id="mydosagedose{{$index}}">
                                                        <div class="pull-left exclude leftdata" onclick="selectdosedata();">4mg </div>
                                                        <div class="pull-right exclude rightclose" ng-show="item.iscustom" ng-click="removedosedata(item.dose)">
                                                            <span class="exclude glyphicon glyphicon-minus-sign  popover-body-data-remove"></span>
                                                        </div>
                                                        <div class="clearfix exclude"></div>
                                                    </div>
                                                    <div class="popover-body-data exclude  dosagedoselistcount" ng-repeat="item in lazyloadingpage.dosagedose.data  |  filter:txtsearchddose:false |  orderBy:'dose'  | limitTo:limitddose" ng-repeat-steps="50"
                                                        data-val="{{item.dose}}" id="mydosagedose{{$index}}">
                                                        <div class="pull-left exclude leftdata" onclick="selectdosedata();">8mg </div>
                                                        <div class="pull-right exclude rightclose" ng-show="item.iscustom" ng-click="removedosedata(item.dose)">
                                                            <span class="exclude glyphicon glyphicon-minus-sign  popover-body-data-remove"></span>
                                                        </div>
                                                        <div class="clearfix exclude"></div>
                                                    </div>
                                                </div>
                                                <!--Normal start -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--- Dose  search  section End-->
                        </div>

                    </div>
                    <div class="pull-left" style="width: 180px; margin-left: 05px; height: 67px">

                        <div style="position: relative">
                            <label>DOSE REGIMEN</label>
                            <input tabindex="6" maxlength="50" id="txtdoseregimen" ng-model="txtdoseregimen" placeholder="Select"
                                ng-enter="pagepophover.enable($event,'doseregimenisopen')"
                                onclick="opendosageform('#regimensel')"
                                ng-keydown="pagepophover.enable($event,'doseregimenisopen')"
                                type="text" class="form-control  select exclude" style="height: 27px;" readonly="true" />


                            <span class="errrotext ">{{txtdoseregimenerror }}
                            </span>


                            <!--- Dose Regimen  search  section start-->

                            <div id="regimensel" class="popover-area exclude {{pagepophover.doseregimenisopen==true?pagepophover.show:pagepophover.hidden}}">
                                <div class="popover exclude ng-isolate-scope bottom fade in bottom-left exclude" style="display: block">
                                    <div class="arrow exclude" style="top: -11px; left: 6px;"></div>

                                    <div style='width: 363px' class="popover-content exclude">
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-title exclude">
                                            <div class="row exclude">
                                                <div class="col-xs-8 col-md-8 col-lg-9 exclude">
                                                    <input type="text" name="txtsearchdoseregimen" id="txtsearchdoseregimen" maxlength="100" placeholder="search"
                                                        ng-model="txtsearchdoseregimen" class="form-control exclude"
                                                        ng-keyup="CityStateSearch($event,'doseregimen')"
                                                        ng-keypress="limitdoseregimen=15;prevententerd($event,'doseregimen');"
                                                        autofocus />

                                                </div>
                                                <div class="col-xs-4 col-md-4 col-lg-3 exclude">
                                                    <input type="button" id="btnadddoseregimen" class="exclude btn btn-primary col-xs-12 col-md-12 col-lg-12 form-control" value="Add" ng-click="adddoseregiment(txtsearchdoseregimen);" style='padding: 2px 10px' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-body exclude">
                                            <div class="exclude" style="height: 200px; overflow-y: scroll" ng-init="limitdoseregimen=15;" id="Div5"
                                                buffered-scroll=" (limitdoseregimen<lazyloadingpage.DosageRegimen.data.length)? limitdoseregimen=limitdoseregimen+10:false">
                                                <!--Normal start -->
                                                <div class="exclude">
                                                    <div class="popover-body-data exclude  dosageregimenlist"
                                                        data-val="{{item.DosageRegimen}}" id="mydosageregimen{{$index}}">
                                                        <div class="pull-left exclude leftdata" onclick="selectdoseregimen();" style="width: 93%">1-1-0  </div>
                                                        <div class="pull-right exclude rightclose" ng-show="item._id" ng-click="removedoseregiment(item)">
                                                            <span class="exclude glyphicon glyphicon-minus-sign  popover-body-data-remove"></span>
                                                        </div>
                                                        <div class="clearfix exclude"></div>
                                                    </div>
                                                    <div class="popover-body-data exclude  dosageregimenlist"
                                                        data-val="{{item.DosageRegimen}}" id="mydosageregimen{{$index}}">
                                                        <div class="pull-left exclude leftdata" onclick="selectdoseregimen();" style="width: 93%">0-1-0  </div>
                                                        <div class="pull-right exclude rightclose" ng-show="item._id" ng-click="removedoseregiment(item)">
                                                            <span class="exclude glyphicon glyphicon-minus-sign  popover-body-data-remove"></span>
                                                        </div>
                                                        <div class="clearfix exclude"></div>
                                                    </div>
                                                    <div class="popover-body-data exclude  dosageregimenlist"
                                                        data-val="{{item.DosageRegimen}}" id="mydosageregimen{{$index}}">
                                                        <div class="pull-left exclude leftdata" onclick="selectdoseregimen();" style="width: 93%">0-0-1  </div>
                                                        <div class="pull-right exclude rightclose" ng-show="item._id" ng-click="removedoseregiment(item)">
                                                            <span class="exclude glyphicon glyphicon-minus-sign  popover-body-data-remove"></span>
                                                        </div>
                                                        <div class="clearfix exclude"></div>
                                                    </div>
                                                </div>
                                                <!--Normal start -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--- Dose  Regimen search  section End-->

                        </div>



                    </div>
                    <div class="pull-left" style="width: 180px; margin-left: 05px; height: 67px">

                        <div style="position: relative">
                            <label>THERAPY DURATION</label>



                            <input tabindex="7" maxlength="50" id="txttherapyduration" ng-model="txttherapyduration" placeholder="Select"
                                onclick="opendosageform('#therapysel')"
                                ng-keydown="pagepophover.enable($event,'Therapydurationisopen')"
                                type="text" class="form-control  select exclude" style="height: 27px;" readonly="true" />











                            <span class="errrotext ">{{txttherapydurationerror }}

                            </span>

                            <div>
                                <!--- Therapy duration  search  section start-->
                                <div id="therapysel" class="popover-area exclude {{pagepophover.Therapydurationisopen==true?pagepophover.show:pagepophover.hidden}}">
                                    <div class="popover exclude ng-isolate-scope bottom fade in bottom-left" style="display: block">
                                        <div class="arrow exclude" style="top: -11px; left: 6px;"></div>

                                        <div style='width: 178px' class="popover-content exclude">
                                            <div class="col-xs-12 col-md-12 col-lg-12 popover-title exclude">
                                                <div class="row exclude exclude">

                                                    <div class="col-xs-12 col-md-12 col-lg-12 exclude">
                                                        <input type="text" name="txttherapysearch" id="txttherapysearch" maxlength="100" placeholder="search"
                                                            ng-model="txttherapysearch" class="form-control exclude"
                                                            ng-keyup="CityStateSearch($event,'therpaydur')"
                                                            ng-keypress="limitstartafter=8;prevententerd($event,'therpaydur');"
                                                            autofocus />
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-12 col-lg-12 popover-body exclude">

                                                <!--Normal start -->

                                                <div class="exclude" style="height: 200px; overflow-y: scroll" ng-init="limitstartafter=10;" id="Div2"
                                                    buffered-scroll=" (limitstartafter<lazyloadingpage.Therapyduration.data.length)? limitstartafter=limitstartafter+10:false">
                                                    <div class="popover-body-data exclude therapydurationlist" ng-repeat="item in lazyloadingpage.Therapyduration.data  |  filter:{value:txttherapysearch} | limitTo:limitstartafter" ng-repeat-steps="50"
                                                        ng-click="selecttheryapuduration();" data-val="{{item.value}}" id="mydosaagetherapy{{$index}}">
                                                        To continue

                                                    </div>
                                                    <div class="popover-body-data exclude therapydurationlist" ng-repeat="item in lazyloadingpage.Therapyduration.data  |  filter:{value:txttherapysearch} | limitTo:limitstartafter" ng-repeat-steps="50"
                                                        ng-click="selecttheryapuduration();" data-val="{{item.value}}" id="mydosaagetherapy{{$index}}">
                                                        6 Months

                                                    </div>
                                                    <div class="popover-body-data exclude therapydurationlist" ng-repeat="item in lazyloadingpage.Therapyduration.data  |  filter:{value:txttherapysearch} | limitTo:limitstartafter" ng-repeat-steps="50"
                                                        ng-click="selecttheryapuduration();" data-val="{{item.value}}" id="mydosaagetherapy{{$index}}">
                                                        3 Months

                                                    </div>
                                                </div>
                                                <!--Normal start -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!---Therapy duration search  section End-->
                            </div>

                        </div>




                    </div>
                    <div class="clearfix"></div>
                </div>


                <div class="" style="margin: 0px 0px; border-top: 1px dotted #9d9d9d; width: 100%; padding: 0px 0px">
                    <div class="pull-left" style="width: 180px;">

                        <div style="position: relative">
                            <label>START AFTER</label>




                            <input tabindex="9" maxlength="50" id="txtstartaftertime" ng-model="txtstartaftertime" placeholder="Select"
                                onclick="opendosageform('#startaftersel')"
                                ng-keydown="pagepophover.enable($event,'Startafterisopen')"
                                type="text" class="form-control  select exclude" style="height: 27px;" readonly="true" />





                            <div>
                                <!--- Therapy duration  search  section start-->

                                <div id="startaftersel" class="popover-area exclude {{pagepophover.Startafterisopen==true?pagepophover.show:pagepophover.hidden}}">
                                    <div class="popover exclude ng-isolate-scope bottom fade in bottom-left" style="display: block">
                                        <div class="arrow" style="top: -11px; left: 6px;"></div>

                                        <div style='width: 178px' class="popover-content exclude">
                                            <div class="col-xs-12 col-md-12 col-lg-12 popover-title exclude">
                                                <div class="row exclude">

                                                    <div class="col-xs-12 col-md-12 col-lg-12 exclude">
                                                        <input type="text" name="txtstartaftersearch" id="txtstartaftersearch" maxlength="100" placeholder="search"
                                                            ng-model="txtstartaftersearch" class="form-control exclude"
                                                            ng-keyup="CityStateSearch($event,'startafters')"
                                                            ng-keypress="limitdoseregimen=15;prevententerd($event,'startafters');"
                                                            autofocus />
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-12 col-lg-12 popover-body exclude">
                                                <div class="exclude" style="height: 200px; overflow-y: scroll" ng-init="startafterlimit=10;" buffered-scroll=" (limit<lazyloadingpage.StartAfter.data.length)? startafterlimit=startafterlimit+10:false">
                                                    <!--Normal start -->
                                                    <div>
                                                        <div class="popover-body-data exclude startafterlist" ng-repeat="item in lazyloadingpage.StartAfter.data |filter:txtstartaftersearch:false | limitTo:startafterlimit" ng-repeat-steps="50"
                                                            ng-click="selectstartafter();" data-val="{{item}}" id="mystartafter{{$index}}">
                                                            <div class="pull-left exclude">1 Day </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="popover-body-data exclude startafterlist" ng-repeat="item in lazyloadingpage.StartAfter.data |filter:txtstartaftersearch:false | limitTo:startafterlimit" ng-repeat-steps="50"
                                                            ng-click="selectstartafter();" data-val="{{item}}" id="mystartafter{{$index}}">
                                                            <div class="pull-left exclude">2 Days </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="popover-body-data exclude startafterlist" ng-repeat="item in lazyloadingpage.StartAfter.data |filter:txtstartaftersearch:false | limitTo:startafterlimit" ng-repeat-steps="50"
                                                            ng-click="selectstartafter();" data-val="{{item}}" id="mystartafter{{$index}}">
                                                            <div class="pull-left exclude">3 Days </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <!--Normal start -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!---Startafterisopen search  section End-->

                            </div>
                        </div>


                    </div>

                    <div class="pull-left" style="margin-left: 30px; padding-top: 26px">

                        <div class="pull-left" style="margin-right: 30px;">

                            <div class="radio radio-info radio-inline">
                                <input tabindex="10" type="radio" id="dosebeforemeal" ng-model="dosebeforemeal" value="1" name="dosagetime" checked="">
                                <label for="dosebeforemeal" class="dosagetime">Before meal </label>
                            </div>



                        </div>

                        <div class="pull-left" style="margin-right: 30px">


                            <div class="radio radio-info radio-inline" style="margin: 0px">
                                <input type="radio" tabindex="10" id="dosebewithmeal" ng-model="dosebewithmeal" value="2" name="dosagetime" checked="">
                                <label for="dosebewithmeal" class="dosagetime">With meal </label>
                            </div>


                        </div>

                        <div class="pull-left" style="margin-right: 30px">

                            <div class="checkbox checkbox-primary" style="margin: 0px">
                                <input id="dosereminder" tabindex="11" name="dosereminder" ng-model="dosereminder" class="styled" type="checkbox">
                                <label for="dosereminder" class="dosagetime">
                                    Dose reminder
                                </label>
                            </div>
                        </div>

                        <div class="pull-left" style="margin-right: 30px">

                            <div class="checkbox checkbox-primary" style="margin: 0px">
                                <input id="dosecomment" tabindex="11" name="dosecomment" ng-model="dosecomment" class="styled" type="checkbox">
                                <label for="dosecomment" class="dosagetime">
                                    Add comment
                                </label>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>

                    <div class="pull-right" style="width: 120px; margin-left: 30px; padding-top: 25px">



                        <button type="submit" tabindex="8" id="adddosebtn" style="width: 120px; height: 27px; font-size: 13px; padding: 0px" ng-click="doseaddclick()" class="btn btn-default quicknavbtn">
                            Add
                        </button>


                    </div>
                    <div class="clearfix"></div>

                    <div style="margin: 10px 0px">

                        <input ng-show="dosecomment" tabindex="12" maxlength="50" id="txtdosecomment" ng-keydown="detectnextfocus($event,'adddosebtn')" ng-model="txtdosecomment" placeholder="Comment" type="text" class="form-control  " />

                    </div>
                </div>


                <div class="" style="margin-top: 15px">

                    <div style="height: 10px; border-top: 1px dotted #9d9d9d; width: 100%">
                    </div>



                    <div class="" ng-repeat="pre in prescriptiondata" style="padding: 4px; border-bottom: 1px dotted #9d9d9d; width: 100%">

                        <div class="pull-left" style="width: 580px">

                            <div style="font-family: RobotoLight; color: #4d4d4d">
                                <label style="color: #282828">Tab Crocin </label>
                                <span style="margin-left: 40px">1000 mg</span>
                            </div>



                            <div style="font-family: RobotoLight; color: #4d4d4d">Paracetamol-1000 mg</div>
                            <div style="font-family: RobotoLight; color: #4d4d4d">1 Tablet of Crocin to be taken After breakfast for <span style="font-family: RobotoRegular; font-size: 13px; color: #4b4a4a; font-weight: 600; margin-left: 02px">6 Months</span></div>

                            <div ng-show="pre.Startafter" style="font-family: RobotoLight; color: #4d4d4d">
                                <label style="color: #282828">START AFTER  (1 days) </label>
                            </div>


                            <div style="margin: 10px 0px;" ng-show="pre.comment">Comments : <span style="font-family: RobotoLight; color: #4d4d4d">Take on time</span> </div>
                        </div>

                        <div class="pull-left" style="">

                            <div class="pull-left txttimetakenperiod" style="width: 100px;">
                                1-0-0

                            </div>

                            <div class="pull-left txttimetakenperiod" style="width: 10px;">
                                X

                            </div>

                            <div class="pull-left txttimetakenperiod" style="width: 90px;">
                                6 Month

                            </div>


                            <div class="pull-right" style="width: 130px;">
                                <button style="font-size: 11px; color: #748013; border: 1px solid #dedede; padding: 02px 14px; text-decoration: none; border-radius: 2px; background: #fff;"
                                    ng-click="editprescription(pre,$index)">
                                    Edit
                                </button>
                                <button style="font-size: 11px; color: #be3131; border: 1px solid #dedede; padding: 02px 14px; text-decoration: none; border-radius: 2px; background: #fff;"
                                    ng-click="removeprescription(pre,$index)">
                                    Remove
                                </button>





                            </div>




                            <div class="clearfix"></div>
                        </div>


                        <div class="clearfix"></div>

                    </div>


                </div>
            </div>

        </div>
    </div>


</asp:Content>

