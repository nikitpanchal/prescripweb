﻿<%@ Page Title="" Language="C#" MasterPageFile="~/views/patient.master" AutoEventWireup="true" CodeFile="patient-prescriptions.aspx.cs" Inherits="views_patient_prescriptions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../css/master.css" rel="stylesheet" />
    <style class="ng-scope">
   
 .patientHomepage .dynamic-grid{
        position: relative;
        display: none;
    }

  .patientHomepage  .dynamic-grid.angular-grid{
        display: block;
    }

  .patientHomepage  .grid {
        border: 1px solid #d0d0d0;
        position: absolute;
        list-style: none;
        background: #ffffff;
        box-sizing: border-box;
        -moz-box-sizing : border-box;
        -webkit-transition: all 0.6s ease-out; 
        transition: all 0.6s ease-out; 
        border-radius:2px;    box-shadow: rgb(208, 208, 208) 0px 1px 4px;
    }
  .patientHomepage  .grid-img {
        width: 100%;
        vertical-align: middle;
        -webkit-transition: opacity 0.6s ease-out;  
        transition: opacity 0.6s ease-out;
        background-color: #fff;
        opacity: 0; 
        visibility: hidden;
    }

   .patientHomepage .grid-img.img-loaded{
        visibility: visible;
        opacity: 1;
    }

    .patientHomepage .reviseddate{
    font-size:13px;color:#1ca4dd;
  
    font-weight:bold;
    }
    .patientHomepage .patientdosage{
    margin:5px 0px 10px 0px;font-size: 16px; color:#424242
    }



    .Userhomefooterseection
    {
    width:250px;
    margin:0px auto;
    background:#1ca4de;
    color:#fff;
    border-radius:5px;
    border-top:0px;
    box-shadow: 1px -5px 20px #ccc5c5;
      padding: 0px 0px;
      height:57px;
    }

    .Userhomefooterseection .lbl-userquciknav {
      text-decoration:none; color:#fff !important;
      font-size:15px;
      font-family: RobotoLight;
      line-height:14px;
    }

    .Userhomefooterseection .bottom-nav {
        width: 165px;
        float: left;
        padding:15px 0px;
        height:56px;


      
    }
        .Userhomefooterseection .lbl-bottomtxt {
             font-weight: bold;
    font-size: 13px;
        }
    
        .Userhomefooterseection .bottom-left-border {
                                     border-right: 1px solid #C0C0C0;
                                     height:30px;
                                }
        .Userhomefooterseection .nav-noborder {
        border:0px;
        }
    .Userhomefooterseection .nav-bottomarea {
    width:170px
    
    }

    .linkbutton{
    color:#f15f0f; border:1px solid #f15f0f;padding:05px 20px;text-decoration:none;border-radius:2px;
    background:#fff;
    }
    .linkbutton:hover{
    color:#1ca4d6; border:1px solid #1ca4d6;padding:05px 20px;text-decoration:none;border-radius:2px
    }

</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="fixed-bottom ng-hide-add" style="width: 100%;">

            <div class="container-fluid Userhomefooterseection" style="">

                <div class="">

                  
                
                    


                        <div class="nav-bottomarea">
                            
                            
                           <a href="add-prescription.aspx" class="lbl-userquciknav" tabindex="0" accesskey="p">
                            <div class="bottom-nav">
                                
                                <div class="bottom-left-border">
                                 <div class="bottom-nav-labelbox">
                                     new 
                                         <div class="lbl-bottomtxt">PRESCRIPTION</div>
                                           
                                 </div>
                               </div>
                                 
                            </div>
                              </a>
                          
                             

                             <div class="clearfix"></div>


                        </div>

                  

                    <div class="clearfix"></div>


                    </div>
                 <div class="clearfix"></div>
                </div>
            </div>
</asp:Content>

