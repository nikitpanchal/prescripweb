﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="add-patient.aspx.cs" Inherits="views_add_patient" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../js/jquery-3.5.1.min.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/master.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/addpatient.js"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/add-patient.css" rel="stylesheet" />
    <link href="../css/master.css" rel="stylesheet" />

    <link href="../css/font-awesome.css" rel="stylesheet" />

    <style>
        .active {
            color: #f15f0f;
            font-weight: bold;
        }


        label {
            font-size: 13px;
            color: #424242;
            margin: 10px 0px;
        }

        .navheaderlbl {
            margin-top: 11px;
            margin-bottom: 15px;
        }

        .deactive-nav {
            color: #424242;
            font-size: 15px;
        }

        .active-nav {
            border: 0px !important;
            font-size: 15px;
            font-family: 'RobotoRegular' !important;
            color: #f15f0f;
            font-weight: bold;
        }

        .navsection {
            width: 150px;
        }

        .quicknavbtn {
            width: 100%;
            height: 35px;
            font-size: 14px;
            font-family: RobotoRegular;
            padding: 07px;
        }

        .form-group {
            height: 63px;
            margin-bottom: 08px;
        }

        button.btn {
            height: 45px;
            font-size: 16px;
        }

        .pageWidthSize {
            width: 900px;
            margin: 0px auto;
            position: relative;
        }

        .pagesizenavtxt {
            text-align: left;
            text-align: center;
            width: 680px;
        }

        @media only screen and (min-width:1400px) {

            .pageWidthSize {
                width: 1200px;
                margin: 0px auto;
                position: relative;
            }

            .pagesizenavtxt {
                text-align: left;
                text-align: center;
                width: 950px;
            }
        }


        @media only screen and (min-width:1860px) {

            .pageWidthSize {
                width: 1500px;
                margin: 0px auto;
                position: relative;
            }

            .pagesizenavtxt {
                text-align: left;
                text-align: center;
                width: 1250px;
            }
        }

        .form-control {
        }

        .top-nav-show {
            height: 70px;
            width: 70px;
            font-size: 45px;
            padding-top: 20px;
        }
    </style>
</head>
<body>
    <header>
        <!-- Navigation -->
        <nav class="navbar navbar-inverse" role="navigation">
            <!-- Page Header -->
            <div class="container" style="width: 100%; margin: 0px auto;">
                <div class="navbar-topspaace">
                    <div class="row">
                        <div class="pull-left">
                            <div style="width: 200px; padding-left: 10px">
                                <a href="/views/home.aspx" class="home_logolink">
                                    <h1 class="prescrip-logotext">
                                        <img src="../images/pcd_logo.png">
                                    </h1>
                                </a>
                            </div>
                        </div>
                        <div class="pull-left " style="text-align: center; display: inline-block; width: 66%">
                            <div style="padding-top: 15px">
                                <h2 style="color: #000000; margin: 0px; font-size: 25px;">Create new patient
                                </h2>
                            </div>

                        </div>

                        <div class="pull-right " style="width: 55px">

                            <div style="text-align: right; cursor: pointer; outline: none; padding-top: 15px; width: 30px; margin: 0px auto" class="drawer-btn exclude" ng-click="openslidebar()" tabindex="-1">

                                <div class="drawer-btn exclude" style="background-image: url('content/images/Menu-Right-icon.png'); height: 34px; background-position: center; background-repeat: no-repeat;">
                                </div>

                            </div>


                        </div>


                    </div>
                </div>

                <div class="clearfix"></div>
            </div>

        </nav>
    </header>
    <form name="form" ng-model="form" novalidate role="form" autocomplete="off">

        <div class="containerx" style="position: relative">



            <div style="position: absolute; width: 100%; top: 70%; height: 1.5px; background: #DFDFDF;">
            </div>

            <div class="pageWidthSize">

                <div class="">

                    <div class=" topquicknav">

                        <div class="pull-left" style="text-align: left; text-align: center">

                            <div class="" style="margin-top: 63px; width: 105px">
                                <input id="backform" type="button" class="btn btn-default quicknavbtn hidden" value="Back" />
                            </div>



                        </div>


                        <div class="pull-left pagesizenavtxt">

                            <div style="width: 350px; margin: 0px auto">

                                <div class="pull-left navsection">


                                    <div id="bdheader" class="active-nav no-border navheaderlbl">Basic Details</div>
                                    <div id="numbd" class="active top-nav-show">
                                        1
                                    </div>

                                </div>


                                <div class="pull-left navsection">


                                    <div class="no-border navheaderlbl addetls">Additional Details</div>
                                    <div class="top-nav-show addetlnum">
                                        2
                                    </div>

                                </div>





                                <div class="clearfix"></div>

                            </div>

                        </div>

                        <div class="pull-right" style="text-align: left; text-align: center">
                            <div class="" style="margin-top: 63px; width: 105px">
                                <input type="button" id="btnfinish" tabindex="15" onclick="finishbtn()" class="btn btn-default quicknavbtn" value="Finish" />
                            </div>

                        </div>

                        <div class="clearfix"></div>


                    </div>


                </div>
            </div>

        </div>

        <div class="row pageWidthSize">



            <div id="form1" class=" " style="height: 500px;">



                <div class="controls">

                    <div class="col-lg-12">

                        <label>PERSONAL DETAILS <span style="color: red">*</span></label>


                    </div>

                    <div class="col-xs-6 col-md-6 col-lg-6">



                        <div class="form-group">

                            <input type="text" maxlength="50" tabindex="1" name="txtfname" id="txtfname" class="form-control" placeholder="Full Name *" autofocus="autofocus" />
                            <span class="errrotext">Please provide full name </span>

                        </div>




                    </div>

                    <div class="col-xs-6 col-md-6 col-lg-6">
                        <div class="row">


                            <div class="col-xs-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <input type="text" maxlength="50" tabindex="2" name="txtpatientId" id="txtpatientId" class="form-control" ng-model="DoctorAddedPatientID" placeholder="Patient id" />
                                </div>

                            </div>
                            <div class="form-group">
                                <span class="form-label">Gender *</span>
                                <div>
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="inlineRadioM" tabindex="3" ng-model="sex" value="0" name="radioInline" checked="" />
                                        <label for="inlineRadioM">Male </label>
                                    </div>

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="inlineRadioF" ng-model="sex" value="1" name="radioInline" checked="" />
                                        <label for="inlineRadioF">Female </label>
                                    </div>

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="inlineRadioS" ng-model="sex" value="2" name="radioInline" checked="" />
                                        <label for="inlineRadioS">Other </label>
                                    </div>
                                </div>



                            </div>

                        </div>
                    </div>

                    <div class="col-xs-6 col-md-6 col-lg-6">

                        <div class="row">

                            <div class="col-xs-4 col-md-4 col-lg-4">


                                <input type="text" name="txtage" tabindex="7" maxlength="10" id="txtage" class="form-control" ng-model="txtage" placeholder="Age" required />
                                <span class="errrotext ">{{ ((submitted || form.txtage.$touched) && form.txtage.$error.required)? "Please enter age":""}} </span>

                            </div>
                            <div class="col-xs-4 col-md-4 col-lg-4">
                                <div class="form-group">



                                    <select tabindex="12" ng-model="txthght" name="txthght" ng-change="BMICal()" id="txthght" class="form-control select" style="padding: 6px 10px">
                                        <option value="year" selected="selected">Years</option>
                                        <option value="month">Day</option>
                                        <option value="day">Month</option>
                                    </select>


                                </div>
                            </div>
                            <div class="col-xs-4 col-md-4 col-lg-4">
                                <span class="form-label">Date of birth</span>

                                <div>
                                    05 June 2020
                                </div>

                            </div>

                        </div>


                    </div>
                </div>


                <div class="controls">

                    <div class="col-lg-12">

                        <label>CONTACT DETAILS</label>
                    </div>


                    <div class="col-xs-6 col-md-6 col-lg-6">

                        <div class="row">
                            <div class="col-xs-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <input type="text" maxlength="10" tabindex="9" minlength="10" ng-minlength="10" ng-maxlength="10" ng-pattern="/^\d{10}$/" name="txtmobile" id="txtmobile" class="form-control" ng-model="txtmobile" placeholder="Mobile" numbers-only />

                                    <span class="errrotext ">{{ ((submitted|| form.txtmobile.$touched) &&((form.txtmobile.$error.minlength || form.txtmobile.$error.maxlength) || form.txtmobile.$dirty && form.txtmobile.$invalid || form.txtmobile.$error.pattern))?"Mobile number should be 10 digits":""}} </span>

                                </div>
                            </div>

                            <div class="col-xs-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <input type="text" maxlength="15" tabindex="10" name="txtlandline" id="txtrefferedby" class="form-control" placeholder="Referred by" />

                                    <span class="errrotext ">{{ ((submitted|| form.txtlandline.$touched) &&(form.txtlandline.$dirty && form.txtlandline.$invalid || form.txtlandline.$error.pattern))?"Please enter landline":""}} </span>

                                </div>
                            </div>

                        </div>



                    </div>
                </div>

                <div class="controls">

                    <div class="col-lg-12">

                        <label>BODY METRICS</label>
                    </div>

                    <div class="col-xs-12 col-md-12 col-lg-12">


                        <div class="row">
                            <div class="col-xs-3 col-md-3 col-lg-3">
                                <div class="form-group">



                                    <select tabindex="12" ng-model="txthght" name="txthght" ng-change="BMICal()" id="bloodgroup" class="form-control select" style="padding: 6px 10px">
                                        <option value="" selected="selected">Select blood group</option>
                                        <option value="O+">O+</option>
                                        <option value="A+">A+</option>
                                        <option value="B+">B+</option>
                                        <option value="AB+">AB+</option>
                                        <option value="A-">A-</option>
                                        <option value="O-">O-</option>
                                        <option value="B-">B-</option>
                                        <option value="AB-">AB-</option>
                                    </select>


                                </div>
                            </div>

                            <div class="col-xs-3 col-md-3 col-lg-3">

                                <div class="form-group">

                                    <input tabindex="11" maxlength="40" id="txtHeight"
                                        name="txtWeight" type="text" placeholder="Height Cm(s)"
                                        class="form-control exclude"
                                        autocomplete="off" />










                                </div>
                            </div>
                            <div class="col-xs-3 col-md-3 col-lg-3">

                                <div class="form-group">

                                    <input tabindex="11" maxlength="40" id="txtWeight"
                                        name="txtWeight" type="text" placeholder="Weight (kg)"
                                        class="form-control exclude"
                                        autocomplete="off" />


                                </div>
                            </div>

                            <div class="col-xs-3 col-md-3 col-lg-3" style="font-family: RobotoLight; font-size: 13px; margin-top: 12px;">
                                <span>BMI =  {{BMIvalue}} </span>
                            </div>
                            <div class="col-xs-3 col-md-3 col-lg-3 text-right" style="color: #908f8f; font-family: RobotoLight; font-size: 13px;">

                                <p><span style="color: red">*</span> FIELDS ARE MANDATORY</p>

                            </div>
                        </div>
                    </div>



                </div>

                <div class="controls" style="">

                    <div class="col-lg-12">
                        <button type="button" tabindex="13" onclick="formsubmitstepone()" class="btn col-xs-6 col-md-6 col-lg-6 btn-primary  col-xs-offset-3" style="">Next</button>

                        <div class="clearfix"></div>

                    </div>
                    <div class="clearfix"></div>


                </div>


            </div>





            <div id="form3" class="hidden">
                <div class="controls">

                    <div class="col-lg-12">

                        <label>CONTACT DETAILS</label>
                    </div>

                    <div class="col-xs-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <input type="text" maxlength="50" name="txtallergy" id="txtlocality" class="form-control" placeholder="Locality" />
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <input type="text" maxlength="50" name="txtdrug" id="txtaddress" class="form-control" placeholder="Address" />
                        </div>
                    </div>



                    <div class="col-xs-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <input type="text" maxlength="50" name="txtfood" id="txtcity" class="form-control" placeholder="City" />
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <input type="text" maxlength="200" name="txtother" id="txtcountry" class="form-control" placeholder="Country" />
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <input type="text" maxlength="200" name="txtother" id="txtemail" class="form-control" placeholder="Email Address" />
                        </div>
                    </div>

                </div>
                <div class="controls" style="margin-bottom: 20px; margin-top: 10px;">

                    <div class="col-lg-12">

                        <div class="row">





                            <div class="col-xs-3 col-md-3 col-lg-3">
                                <span>Alcohol</span>
                                <div>
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="AlcoholY" ng-model="Alcohol" value="Yes" name="Alcohol">
                                        <label for="AlcoholY">Yes </label>
                                    </div>

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="AlcoholN" ng-model="Alcohol" value="No" name="Alcohol">
                                        <label for="AlcoholN">No </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-3 col-md-3 col-lg-3">
                                <span>Tobacco</span>

                                <div>
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="TobaccoY" ng-model="Tobacco" value="Yes" name="Tobacco">
                                        <label for="TobaccoY">Yes </label>
                                    </div>

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="TobaccoN" ng-model="Tobacco" value="No" name="Tobacco">
                                        <label for="TobaccoN">No </label>
                                    </div>
                                </div>


                            </div>


                        </div>





                    </div>

                    <div class="clearfix"></div>

                </div>



                <div class="controls" style="margin-bottom: 20px;">

                    <div class="col-lg-12">
                        <label>FAMILY HISTORY</label>
                    </div>
                    <div class="col-lg-12">

                        <div class="row">

                            <div class="col-xs-3 col-md-3 col-lg-3">
                                <span>Diabetes</span>
                                <div class="">

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="FAMILYDiabetesY" ng-model="FAMILYDiabetes" value="Yes" name="FAMILYDiabetes">
                                        <label for="FAMILYDiabetesY">Yes </label>
                                    </div>

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="FAMILYDiabetesN" ng-model="FAMILYDiabetes" value="No" name="FAMILYDiabetes">
                                        <label for="FAMILYDiabetesN">No </label>
                                    </div>


                                </div>
                            </div>

                            <div class="col-xs-3 col-md-3 col-lg-3">
                                <span>Hypertension</span>
                                <div class="">

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="FAMILYHypertensionY" ng-model="FAMILYHypertension" value="Yes" name="FAMILYHypertension">
                                        <label for="FAMILYHypertensionY">Yes </label>
                                    </div>

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="FAMILYHypertensionN" ng-model="FAMILYHypertension" value="No" name="FAMILYHypertension">
                                        <label for="FAMILYHypertensionN">No </label>
                                    </div>


                                </div>
                            </div>

                            <div class="col-xs-3 col-md-3 col-lg-3">
                                <span>Heart Disease</span>
                                <div class="p">

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="FAMILYHeartDiseaseY" ng-model="FAMILYHeartDisease" value="Yes" name="FAMILYHeartDisease">
                                        <label for="FAMILYHeartDiseaseY">Yes </label>
                                    </div>

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="FAMILYHeartDiseaseN" ng-model="FAMILYHeartDisease" value="No" name="FAMILYHeartDisease">
                                        <label for="FAMILYHeartDiseaseN">No </label>
                                    </div>


                                </div>
                            </div>

                            <div class="col-xs-3 col-md-3 col-lg-3">

                                <div class="">
                                    <input type="text" name="Familytxtother" maxlength="200" id="Familytxtother" class="form-control" ng-model="Familytxtother" placeholder="Other" />

                                </div>
                            </div>

                        </div>





                    </div>


                    <div class="clearfix"></div>

                </div>




                <div class="controls" style="margin-bottom: 20px;">

                    <div class="col-lg-12">
                        <label>PERSONAL HISTORY</label>
                    </div>
                    <div class="col-lg-12">

                        <div class="row">

                            <div class="col-xs-3 col-md-3 col-lg-3">
                                <span>Diabetes</span>
                                <div class="">

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="PERSONALDiabetesY" ng-model="PERSONALDiabetes" value="Yes" name="PERSONALDiabetes" />
                                        <label for="PERSONALDiabetesY">Yes </label>
                                    </div>

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="PERSONALDiabetesN" ng-model="PERSONALDiabetes" value="No" name="PERSONALDiabetes" />
                                        <label for="PERSONALDiabetesN">No </label>
                                    </div>


                                </div>
                            </div>

                            <div class="col-xs-3 col-md-3 col-lg-3">
                                <span>Hypertension</span>
                                <div class="">

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="PERSONALHypertensionY" ng-model="PERSONALHypertension" value="Yes" name="PERSONALHypertension" />
                                        <label for="PERSONALHypertensionY">Yes </label>
                                    </div>

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="PERSONALHypertensionN" ng-model="PERSONALHypertension" value="No" name="PERSONALHypertension" />
                                        <label for="PERSONALHypertensionN">No </label>
                                    </div>


                                </div>
                            </div>

                            <div class="col-xs-3 col-md-3 col-lg-3">
                                <span>Heart Disease</span>
                                <div class="">

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="PERSONALHeartDiseaseY" ng-model="PERSONALHeartDisease" value="Yes" name="PERSONALHeartDisease">
                                        <label for="PERSONALHeartDiseaseY">Yes </label>
                                    </div>

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="PERSONALHeartDiseaseN" ng-model="PERSONALHeartDisease" value="No" name="PERSONALHeartDisease">
                                        <label for="PERSONALHeartDiseaseN">No </label>
                                    </div>


                                </div>
                            </div>

                            <div class="col-xs-3 col-md-3 col-lg-3">

                                <div class="">
                                    <input type="text" name="PERSONALtxtother" maxlength="200" id="PERSONALtxtother" class="form-control" ng-model="PERSONALtxtother" placeholder="Other" />

                                </div>
                            </div>

                        </div>


                    </div>


                    <div class="clearfix"></div>

                </div>


                <div class="controls">

                    <div class="col-lg-12">

                        <label>ALLERGY INFORMATION</label>
                    </div>

                    <div class="col-xs-4 col-md-4 col-lg-4">

                        <div class="form-group">


                            <input tabindex="11" maxlength="40" id="txtenvoriment"
                                name="txtenvoriment" type="text" placeholder="Envorimental"
                                class="form-control select exclude" readonly="true" autocomplete="off" />



                            <!---City section start-->
                            <div class="popover-area {{pagepophover.bodymetisOpen==true?pagepophover.show:pagepophover.hidden}}" style="top: -110px; left: 140px; width: 200px">
                                <div class="popover ng-isolate-scope right fade in exclude" style="display: block">
                                    <div class="arrow" style="left: -11px !important"></div>
                                    <div style='width: 230px' class="popover-content exclude">
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-title exclude">
                                            <div class="row exclude">
                                                <div class=" exclude " style="margin: 0px 10px 0px 10px; float: left">
                                                    <input type="text" tabindex="11" name="Weightsearch" id="Weightsearch" placeholder="search" ng-keyup="CityStateSearch($event,'weight')" ng-keypress="limit=15;prevententerd($event,'weight');" ng-model="Weightsearch" class="form-control  col-xs-12 col-md-12 col-lg-12 exclude " />
                                                </div>
                                                <div class="exclude" style="margin: 0px; float: left;">
                                                    <input id="btnaddweight" tabindex="11" type="button" class="exclude btn btn-primary col-xs-12 col-md-12 col-lg-12 form-control" value="Add" ng-click="addWeight(Weightsearch,$event);Weightsearch='' " />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-body exclude">
                                            <div id="bodypopoverscroll" class="exclude" style="height: 200px; overflow-y: scroll" ng-init="limit=30;" buffered-scroll=" (limit<WeightList.length)? limit=limit+150:false ">

                                                <div class="exclude popover-body-data myweightlisttotal"
                                                    ng-click="selectedWeight(item,$event);">
                                                    <div class="exclude">Dust allergy </div>
                                                    <div class="clearfix "></div>

                                                </div>
                                                <div class="exclude popover-body-data myweightlisttotal"
                                                    ng-click="selectedWeight(item,$event);">
                                                    <div class="exclude">Dust allergy </div>
                                                    <div class="clearfix "></div>

                                                </div>
                                                <div class="exclude popover-body-data myweightlisttotal"
                                                    ng-click="selectedWeight(item,$event);">
                                                    <div class="exclude">Dust allergy </div>
                                                    <div class="clearfix "></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                </div>
                            </div>
                            <!--- State section End-->




                        </div>
                    </div>

                    <div class="col-xs-4 col-md-4 col-lg-4">

                        <div class="form-group">


                            <input tabindex="11" maxlength="40" id="txtfood"
                                name="txtfood" type="text" placeholder="Food"
                                class="form-control select exclude" readonly="true" autocomplete="off" />



                            <!---City section start-->
                            <div class="popover-area {{pagepophover.bodymetisOpen==true?pagepophover.show:pagepophover.hidden}}" style="top: -110px; left: 140px; width: 200px">
                                <div class="popover ng-isolate-scope right fade in exclude" style="display: block">
                                    <div class="arrow" style="left: -11px !important"></div>
                                    <div style='width: 230px' class="popover-content exclude">
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-title exclude">
                                            <div class="row exclude">
                                                <div class=" exclude " style="margin: 0px 10px 0px 10px; float: left">
                                                    <input type="text" tabindex="11" name="Weightsearch" id="Weightsearch" placeholder="search" ng-keyup="CityStateSearch($event,'weight')" ng-keypress="limit=15;prevententerd($event,'weight');" ng-model="Weightsearch" class="form-control  col-xs-12 col-md-12 col-lg-12 exclude " />
                                                </div>
                                                <div class="exclude" style="margin: 0px; float: left;">
                                                    <input id="btnaddweight" tabindex="11" type="button" class="exclude btn btn-primary col-xs-12 col-md-12 col-lg-12 form-control" value="Add" ng-click="addWeight(Weightsearch,$event);Weightsearch='' " />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-body exclude">
                                            <div id="bodypopoverscroll" class="exclude" style="height: 200px; overflow-y: scroll" ng-init="limit=30;" buffered-scroll=" (limit<WeightList.length)? limit=limit+150:false ">

                                                <div class="exclude popover-body-data myweightlisttotal"
                                                    ng-click="selectedWeight(item,$event);">
                                                    <div class="exclude">Dust allergy </div>
                                                    <div class="clearfix "></div>

                                                </div>
                                                <div class="exclude popover-body-data myweightlisttotal"
                                                    ng-click="selectedWeight(item,$event);">
                                                    <div class="exclude">Dust allergy </div>
                                                    <div class="clearfix "></div>

                                                </div>
                                                <div class="exclude popover-body-data myweightlisttotal"
                                                    ng-click="selectedWeight(item,$event);">
                                                    <div class="exclude">Dust allergy </div>
                                                    <div class="clearfix "></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                </div>
                            </div>
                            <!--- State section End-->




                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-lg-4">

                        <div class="form-group">


                            <input tabindex="11" maxlength="40" id="txtdrug"
                                name="txtdrug" type="text" placeholder="Drugs"
                                class="form-control select exclude" readonly="true" autocomplete="off" />



                            <!---City section start-->
                            <div class="popover-area {{pagepophover.bodymetisOpen==true?pagepophover.show:pagepophover.hidden}}" style="top: -110px; left: 140px; width: 200px">
                                <div class="popover ng-isolate-scope right fade in exclude" style="display: block">
                                    <div class="arrow" style="left: -11px !important"></div>
                                    <div style='width: 230px' class="popover-content exclude">
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-title exclude">
                                            <div class="row exclude">
                                                <div class=" exclude " style="margin: 0px 10px 0px 10px; float: left">
                                                    <input type="text" tabindex="11" name="Weightsearch" id="Weightsearch" placeholder="search" ng-keyup="CityStateSearch($event,'weight')" ng-keypress="limit=15;prevententerd($event,'weight');" ng-model="Weightsearch" class="form-control  col-xs-12 col-md-12 col-lg-12 exclude " />
                                                </div>
                                                <div class="exclude" style="margin: 0px; float: left;">
                                                    <input id="btnaddweight" tabindex="11" type="button" class="exclude btn btn-primary col-xs-12 col-md-12 col-lg-12 form-control" value="Add" ng-click="addWeight(Weightsearch,$event);Weightsearch='' " />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-body exclude">
                                            <div id="bodypopoverscroll" class="exclude" style="height: 200px; overflow-y: scroll" ng-init="limit=30;" buffered-scroll=" (limit<WeightList.length)? limit=limit+150:false ">

                                                <div class="exclude popover-body-data myweightlisttotal"
                                                    ng-click="selectedWeight(item,$event);">
                                                    <div class="exclude">Dust allergy </div>
                                                    <div class="clearfix "></div>

                                                </div>
                                                <div class="exclude popover-body-data myweightlisttotal"
                                                    ng-click="selectedWeight(item,$event);">
                                                    <div class="exclude">Dust allergy </div>
                                                    <div class="clearfix "></div>

                                                </div>
                                                <div class="exclude popover-body-data myweightlisttotal"
                                                    ng-click="selectedWeight(item,$event);">
                                                    <div class="exclude">Dust allergy </div>
                                                    <div class="clearfix "></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                </div>
                            </div>
                            <!--- State section End-->




                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-lg-4">

                        <div class="form-group">


                            <input tabindex="11" maxlength="40" id="txtotherallergy"
                                name="txtotherallergy" type="text" placeholder="Others"
                                class="form-control select exclude" readonly="true" autocomplete="off" />



                            <!---City section start-->
                            <div class="popover-area {{pagepophover.bodymetisOpen==true?pagepophover.show:pagepophover.hidden}}" style="top: -110px; left: 140px; width: 200px">
                                <div class="popover ng-isolate-scope right fade in exclude" style="display: block">
                                    <div class="arrow" style="left: -11px !important"></div>
                                    <div style='width: 230px' class="popover-content exclude">
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-title exclude">
                                            <div class="row exclude">
                                                <div class=" exclude " style="margin: 0px 10px 0px 10px; float: left">
                                                    <input type="text" tabindex="11" name="Weightsearch" id="Weightsearch" placeholder="search" ng-keyup="CityStateSearch($event,'weight')" ng-keypress="limit=15;prevententerd($event,'weight');" ng-model="Weightsearch" class="form-control  col-xs-12 col-md-12 col-lg-12 exclude " />
                                                </div>
                                                <div class="exclude" style="margin: 0px; float: left;">
                                                    <input id="btnaddweight" tabindex="11" type="button" class="exclude btn btn-primary col-xs-12 col-md-12 col-lg-12 form-control" value="Add" ng-click="addWeight(Weightsearch,$event);Weightsearch='' " />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12 col-lg-12 popover-body exclude">
                                            <div id="bodypopoverscroll" class="exclude" style="height: 200px; overflow-y: scroll" ng-init="limit=30;" buffered-scroll=" (limit<WeightList.length)? limit=limit+150:false ">

                                                <div class="exclude popover-body-data myweightlisttotal"
                                                    ng-click="selectedWeight(item,$event);">
                                                    <div class="exclude">Dust allergy </div>
                                                    <div class="clearfix "></div>

                                                </div>
                                                <div class="exclude popover-body-data myweightlisttotal"
                                                    ng-click="selectedWeight(item,$event);">
                                                    <div class="exclude">Dust allergy </div>
                                                    <div class="clearfix "></div>

                                                </div>
                                                <div class="exclude popover-body-data myweightlisttotal"
                                                    ng-click="selectedWeight(item,$event);">
                                                    <div class="exclude">Dust allergy </div>
                                                    <div class="clearfix "></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                </div>
                            </div>
                            <!--- State section End-->




                        </div>
                    </div>
                </div>





                <div class="controls" style="">

                    <div class="col-lg-12">
                        <button type="submit" ng-click="formsubmit('form3')" class="btn col-xs-6 col-md-6 col-lg-6 btn-primary  col-xs-offset-3" style="">Finish</button>

                        <div class="clearfix"></div>

                    </div>
                    <div class="clearfix"></div>


                </div>


            </div>



        </div>

    </form>
</body>
</html>
