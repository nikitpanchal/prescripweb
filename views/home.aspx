﻿<%@ Page Title="" Language="C#" MasterPageFile="~/views/MasterPage.master" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../js/patientlist.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>

    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    <style type="text/css">
        .patientphover {
            width: 180px;
            height: 255px;
            background: rgb(240, 240, 240);
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="" style="position: relative">

        <div class="patientdatashow" style="z-index: 55555">


            <div style="width: 545px; margin: 150px auto; text-align: center; display: none; font-family: RobotoLight">
                <img src="../images/user-128.png" />
                <p style="color: #E1E1E1; font-size: 24px; margin: 20px;">You dont seem to have any patient in your list.</p>

                <p style="color: #E1E1E1; font-size: 30px">
                    Click on 
                        <a style="font-family: robotolight; color: #1ca4dd;">add patient 

                        </a>
                    get started
                </p>
            </div>


            <div class="" style="">
                <div id="patientlistmain" class="">
                    <%--  <div class="pull-left" ng-repeat="p in patients" style="width: 180px; margin: 10px 11px 10px 1px; position: relative;">
                        <div class="patientphover" title='{{p.First_Name +" "+ p.Last_Name }}' style="width: 180px; height: 255px; background: rgb(240, 240, 240); padding: 10px;">

                            <div ng-click="onSelectpatient(p)">


                                <div style="height: 160px; background-size: 100%; background-repeat: no-repeat; background-position: center; border-radius: 50%; background-image: url('../images/user-default-icon.png')">
                                </div>
                                <div style="margin-top: 10px; text-transform: uppercase; font-size: 13px; color: #424242; font-weight: 600;">
                                    Nikit Panchal
                                </div>

                                <div style="margin-top: 1px; font-size: 13px">
                                    <div class="pull-left">
                                        28
                                    </div>
                                    <div class="pull-left">
                                        Maile
                                    </div>

                                    <div class="clearfix"></div>

                                </div>
                                <div style="margin-top: 1px; font-size: 13px">
                                    9322562216
                                </div>
                            </div>

                            <button title='Remove {{p.First_Name}}' style="border: 0px; background-color: rgb(240, 240, 240); position: absolute; bottom: 10px; right: 0px">
                                <div onclick="deletepatientprofile(p,$event)" style="background-image: url('images/icon-delete.png'); height: 30px; width: 30px; background-repeat: no-repeat">
                                </div>
                            </button>


                        </div>

                    </div>--%>
                </div>
                <div id="bLoader" style="position: fixed; bottom: 0px; width: 100%;">
                    <img src="../images/bottom-loader.svg" style="vertical-align: middle; width: 100px; height: 100px; margin: 0 auto; display: block;">
                </div>
            </div>

        </div>


    </div>
</asp:Content>

